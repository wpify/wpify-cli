<?php

use PhpParser\BuilderFactory;
use PhpParser\Node;
use PhpParser\Node\Stmt\Use_;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter\Standard;

if (!class_exists('WP_CLI')) {
	return;
}

class WpifyCLI extends WP_CLI_Command
{

	/**
	 * Rename WordPress' database prefix.
	 * You will be prompted for confirmation before the command makes any changes.
	 * ## OPTIONS
	 * <name>
	 * : The model name
	 * [--namespace]
	 * ## EXAMPLES
	 * wp wpify model name
	 *
	 * @param array $args
	 * @param array $assoc_args
	 */
	public function cpt($args, $assoc_args)
	{
		require_once 'vendor/autoload.php';
		$name = strtolower($args[0]);
		$Name = ucfirst($args[0]);
		$cwd  = getcwd();

		if (!file_exists($cwd . '/src/Plugin.php')) {
			WP_CLI::error('Plugin.php not found');
		}
		if (isset($assoc_args['namespace'])) {
			$namespace = $assoc_args['namespace'];
		} else {
			$namespace = $this->extract_namespace($cwd . '/src/Plugin.php');
		}

		$args = [
						'name'      => $name,
						'Name'      => $Name,
						'Namespace' => $namespace,
						'namespace' => strtolower($namespace),
		];
		$m    = new Mustache_Engine(
						[
										'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/templates'),

						]
		);

		$tpl = $m->loadTemplate('Model');

		file_put_contents(
						$cwd . '/src/Models/' . $Name . 'Model.php',
						$tpl->render($args)
		);

		$tpl = $m->loadTemplate('PostType');

		file_put_contents(
						$cwd . '/src/Cpt/' . $Name . 'PostType.php',
						$tpl->render($args)
		);
		$tpl = $m->loadTemplate('Repository');

		file_put_contents(
						$cwd . '/src/Repositories/' . $Name . 'Repository.php',
						$tpl->render($args)
		);

		$manager_path = $cwd . '/src/Managers/CptManager.php';

		$manager = file_get_contents($manager_path);
		$parser  = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
		try {
			$ast = $parser->parse($manager);
		} catch (Error $error) {
			echo "Parse error: {$error->getMessage()}\n";
		}

		$traverser = new NodeTraverser;
		$traverser->addVisitor(new CliVisitor($Name, $namespace, 'Cpt', 'PostType'));

		$modifiedStmts = $traverser->traverse($ast);
		$prettyPrinter = new Standard();
		$code          = $prettyPrinter->prettyPrintFile($modifiedStmts);
		file_put_contents($manager_path, $code);

		$manager_path = $cwd . '/src/Managers/RepositoriesManager.php';
		$manager      = file_get_contents($manager_path);
		$parser       = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
		try {
			$ast = $parser->parse($manager);
		} catch (Error $error) {
			echo "Parse error: {$error->getMessage()}\n";
		}

		$traverser = new NodeTraverser;
		$traverser->addVisitor(new CliVisitor($Name, $namespace, 'Repositories', 'Repository'));

		$modifiedStmts = $traverser->traverse($ast);
		$prettyPrinter = new Standard();
		$code          = $prettyPrinter->prettyPrintFile($modifiedStmts);
		file_put_contents($manager_path, $code);

		WP_CLI::success('CPT added successfuly');
	}

	/**
	 * Rename WordPress' database prefix.
	 * You will be prompted for confirmation before the command makes any changes.
	 * ## OPTIONS
	 * <name> <postType>
	 * : The model name
	 * [--namespace]
	 * ## EXAMPLES
	 * wp wpify model name postType
	 *
	 * @param array $args
	 * @param array $assoc_args
	 *
	 * @throws \WP_CLI\ExitException
	 */
	public function taxonomy($args, $assoc_args)
	{
		require_once 'vendor/autoload.php';
		$name = strtolower($args[0]);
		$Name = ucfirst($args[0]);
		$slug = $this->get_slug($args[0]);
		$Cpt  = ucfirst($args[1]);
		$cwd  = getcwd();

		if (!file_exists($cwd . '/src/Plugin.php')) {
			WP_CLI::error('Plugin.php not found');
		}
		if (isset($assoc_args['namespace'])) {
			$namespace = $assoc_args['namespace'];
		} else {
			$namespace = $this->extract_namespace($cwd . '/src/Plugin.php');
		}

		$args = [
						'name'          => $name,
						'Name'          => $Name,
						'Namespace' => $namespace,
						'namespace' => strtolower($namespace),
						'cpt'           => $Cpt,
						'taxonomy_slug' => $slug,
		];

		$m = new Mustache_Engine(
						[
										'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/templates'),

						]
		);

		$tpl = $m->loadTemplate('Taxonomy');

		file_put_contents(
						$cwd . '/src/Taxonomies/' . $Name . 'Taxonomy.php',
						$tpl->render($args)
		);

		$tpl = $m->loadTemplate('TaxonomyRepository');

		file_put_contents(
						$cwd . '/src/Repositories/' . $Name . 'Repository.php',
						$tpl->render($args)
		);

		$manager_path = $cwd . '/src/Managers/TaxonomiesManager.php';

		$manager = file_get_contents($manager_path);
		$parser  = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
		try {
			$ast = $parser->parse($manager);
		} catch (Error $error) {
			echo "Parse error: {$error->getMessage()}\n";
		}


		$traverser = new NodeTraverser;
		$traverser->addVisitor(new CliVisitor($Name, $namespace, 'Taxonomies', 'Taxonomy'));

		$modifiedStmts = $traverser->traverse($ast);
		$prettyPrinter = new Standard();
		$code          = $prettyPrinter->prettyPrintFile($modifiedStmts);
		file_put_contents($manager_path, $code);

		$manager_path = $cwd . '/src/Managers/RepositoriesManager.php';
		$manager      = file_get_contents($manager_path);
		$parser       = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
		try {
			$ast = $parser->parse($manager);
		} catch (Error $error) {
			echo "Parse error: {$error->getMessage()}\n";
		}

		$traverser = new NodeTraverser;
		$traverser->addVisitor(new CliVisitor($Name, $namespace, 'Repositories', 'Repository'));

		$modifiedStmts = $traverser->traverse($ast);
		$prettyPrinter = new Standard();
		$code          = $prettyPrinter->prettyPrintFile($modifiedStmts);
		file_put_contents($manager_path, $code);

		WP_CLI::success('Taxonomy added successfuly');
	}

	/**
	 * Rename WordPress' database prefix.
	 * You will be prompted for confirmation before the command makes any changes.
	 * ## OPTIONS
	 * <name>
	 * : The model name
	 * [--namespace]
	 * ## EXAMPLES
	 * wp wpify model name
	 *
	 * @param array $args
	 * @param array $assoc_args
	 *
	 * @throws \WP_CLI\ExitException
	 */
	public function block($args, $assoc_args)
	{
		require_once 'vendor/autoload.php';
		$name = strtolower($args[0]);
		$Name = ucfirst($args[0]);
		$slug = $this->get_slug($args[0]);
		$cwd  = getcwd();

		if (!file_exists($cwd . '/src/Plugin.php')) {
			WP_CLI::error('Plugin.php not found');
		}
		if (isset($assoc_args['namespace'])) {
			$Namespace = $assoc_args['namespace'];
		} else {
			$Namespace = $this->extract_namespace($cwd . '/src/Plugin.php');
		}
		$namespace = strtolower($Namespace);


		$args = [
						'name'      => $name,
						'Name'      => $Name,
						'namespace' => $namespace,
						'slug'      => $slug,
						'Namespace' => $Namespace,
		];

		$m = new Mustache_Engine(
						[
										'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/templates'),

						]
		);

		$tpl = $m->loadTemplate('Block/Block.mustache');

		file_put_contents(
						$cwd . '/src/Blocks/' . $Name . 'Block.php',
						$tpl->render($args)
		);
		mkdir($cwd . '/assets/blocks/' . $slug);

		$tpl = $m->loadTemplate('Block/assets/backend.js.mustache');

		file_put_contents(
						$cwd . '/assets/blocks/' . $slug . '/backend.js',
						$tpl->render($args)
		);

		$tpl = $m->loadTemplate('Block/assets/backend.module.scss.mustache');

		file_put_contents(
						$cwd . '/assets/blocks/' . $slug . '/backend.module.scss',
						$tpl->render($args)
		);

		$tpl = $m->loadTemplate('Block/assets/frontend.js.mustache');

		file_put_contents(
						$cwd . '/assets/blocks/' . $slug . '/frontend.js',
						$tpl->render($args)
		);

		$tpl = $m->loadTemplate('Block/assets/frontend.scss.mustache');

		file_put_contents(
						$cwd . '/assets/blocks/' . $slug . '/frontend.scss',
						$tpl->render($args)
		);

		$manager_path = $cwd . '/src/Managers/BlocksManager.php';

		$manager = file_get_contents($manager_path);
		$parser  = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
		try {
			$ast = $parser->parse($manager);
		} catch (Error $error) {
			echo "Parse error: {$error->getMessage()}\n";
		}

		$traverser = new NodeTraverser;
		$traverser->addVisitor(new CliVisitor($Name, $Namespace, 'Blocks', 'Block'));

		$modifiedStmts = $traverser->traverse($ast);
		$prettyPrinter = new Standard();
		$code          = $prettyPrinter->prettyPrintFile($modifiedStmts);
		file_put_contents($manager_path, $code);

		WP_CLI::success('Block added successfuly');
	}

	/**
	 * Extract namespace from file
	 *
	 * @param $file
	 *
	 * @return string|null
	 */
	private function extract_namespace($file)
	{
		$ns     = null;
		$handle = fopen($file, "r");
		if ($handle) {
			while (($line = fgets($handle)) !== false) {
				if (strpos($line, 'namespace') === 0) {
					$parts = explode(' ', $line);
					$ns    = rtrim(trim($parts[1]), ';');
					break;
				}
			}
			fclose($handle);
		}

		return $ns;
	}

	public function get_slug($input)
	{
		$re = '/(?<=[a-z])(?=[A-Z])/x';
		$a  = preg_split($re, $input);

		return strtolower(join('-', $a));
	}
}

WP_CLI::add_command('wpify', 'WpifyCLI');

class CliVisitor extends NodeVisitorAbstract
{
	private $name;
	private $namespace;
	private $type;
	private $subnamespace;

	public function __construct($name, $namespace, $subnamespace, $type)
	{
		$this->name         = $name;
		$this->namespace    = $namespace;
		$this->type         = $type;
		$this->subnamespace = $subnamespace;
	}

	public function leaveNode(Node $node)
	{
		if ($node instanceof Node\Stmt\Namespace_) {
			$nodes      = $node->stmts;
			$insert_key = 0;
			$inserted   = false;
			foreach ($nodes as $key => $item) {
				if ($item instanceof PhpParser\Node\Stmt\Use_) {
					$insert_key = $key + 1;
					foreach ($item->uses as $use) {
						foreach ($use->name->parts as $part) {
							if ($part === "{$this->name}{$this->type}") {
								$inserted = true;
							}
						}
					}
				}
			}

			if (!$inserted) {
				$name           = "{$this->namespace}\\{$this->subnamespace}\\{$this->name}{$this->type}";
				$useNodeBuilder = new \PhpParser\Builder\Use_($name, Use_::TYPE_NORMAL);
				$before         = array_slice($nodes, 0, $insert_key);
				$after          = array_slice($nodes, $insert_key);
				$node->stmts    = $nodes = array_merge($before, [$useNodeBuilder->getNode()], $after);
			}
		}
		if ($node instanceof Node\Stmt\PropertyProperty) {
			$inserted   = false;
			$class_name = "{$this->name}{$this->type}";
			$name       = $node->name->name;
			if ($name === 'modules') {
				foreach ($node->default->items as $item) {
					foreach ($item->value->class->parts as $part) {
						if ($part === $class_name) {
							$inserted = true;
						}
					}
				}

				if (!$inserted) {
					$factory                = new BuilderFactory();
					$item                   = new Node\Expr\ArrayItem($factory->constFetch("{$class_name}::class"));
					$node->default->items[] = $item;
				}
			}
		}

		return $node;
	}
}
